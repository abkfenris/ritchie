<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package ritchie
 * @since ritchie 1.0
 */

get_header(); ?>
		<div id="main" class="site-main">
			<div id="content" class="site-content" role="main">

			<?php if ( have_posts() ) : ?>

				<?php ritchie_content_nav( 'nav-above' ); ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
					?>

				<?php endwhile; ?>

				<?php ritchie_content_nav( 'nav-below' ); ?>
				<a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Syndicate this site using RSS'); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/41-feed.png" alt="RSS Feed" title="RSS Feed" /> Subscribe</a>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'index' ); ?>

			<?php endif; ?>

			</div><!-- #content .site-content -->
		</div><!-- #main .site-main -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>