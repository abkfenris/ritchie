<?php
/**
 * @package ritchie
 * @since ritchie 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/101-gameplan.png" alt="101-gameplan" width="28" height="28" class="type-icon"/>
			</a>
			<a href="<? echo get_post_meta($post->ID, '_format_link_url', true); ?>"><?php the_title(); ?></a>
		</h1>
		<div class="attribution">from <a href="<? echo get_post_meta($post->ID, '_format_link_url', true); ?>"><? echo get_post_meta($post->ID, '_format_link_url', true); ?></a></div>
	</header><!-- .entry-header -->

	<div class="entry-content left">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	<footer class="entry-meta right">
		<?php ritchie_posted_on(); ?>
	 	<br />
		<?php ritchie_entry_meta();	?>

		<?php edit_post_link( __( 'Edit', 'ritchie' ), '<br /><span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
