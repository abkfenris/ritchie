//number 5 on http://www.ericmmartin.com/5-tips-for-using-jquery-with-wordpress/ to let $ be used
jQuery( document ).ready( function( $ ) {
	// from http://stackoverflow.com/a/2114778
	$(window).load(function() {
		//call the equalize height function
		equalHeight($("#main, #secondary"));
		
		//equalize funciton
		function equalHeight(group) {
			tallest = 0;
			group.each(function() {
				thisHeight = $(this).height();
				if(thisHeight > tallest) {
					tallest = thisHeight;
				}
			});
			$(group).height(tallest);
		}
	});
});
