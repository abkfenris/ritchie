<?php
/**
 * @package ritchie
 * @since ritchie 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/107-widescreen.png" alt="107-widescreen" class="type-icon"/>
				<?php the_title(); ?>
			</a>
		</h1>
		<? echo wp_oembed_get( get_post_meta($post->ID, '_format_video_embed', true), array('width'=>612) ); /* from http://codex.wordpress.org/Function_Reference/wp_oembed_get */ ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'ritchie' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'ritchie' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

<footer class="entry-meta">
		<?php ritchie_short_meta_horizontal(); ?>

		<?php edit_post_link( __( 'Edit', 'ritchie' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
