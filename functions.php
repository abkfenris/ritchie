<?php
/**
 * ritchie functions and definitions
 *
 * @package ritchie
 * @since ritchie 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since ritchie 1.0
 */
if ( ! isset( $content_width ) )
	$content_width = 612; /* pixels */

if ( ! function_exists( 'ritchie_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since ritchie 1.0
 */
function ritchie_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/extras.php' );

	/**
	 * Custom Theme Options
	 */
	//require( get_template_directory() . '/inc/theme-options/theme-options.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on ritchie, use a find and replace
	 * to change 'ritchie' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'ritchie', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails and our sizes
	 */
	add_theme_support( 'post-thumbnails' );
	if (function_exists( 'add_image_size' )) {
		add_image_size('front-thumb', 612, 300, true);
		add_image_size('front-image', 612, 9999);
	}

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'ritchie' ),
		'header'  => __( 'Header Menu', 'ritchie' ),
		'footer'  => __( 'Footer Menu', 'ritchie' ),
	) );

	/**
	 * Add support for the Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'link', 'gallery', 'image', 'video' ) );
	
	/**
	 * Adds the image sizes
	 */

}
endif; // ritchie_setup
add_action( 'after_setup_theme', 'ritchie_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since ritchie 1.0
 */
function ritchie_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'ritchie' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h1 class="widget-title">',
		'after_title' => '</h1>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer', 'ritchie' ),
		'id' => 'sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h1 class="widget-title">',
		'after_title' => '</h1>',
	) );
}
add_action( 'widgets_init', 'ritchie_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function ritchie_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	wp_enqueue_script( 'small-menu', get_template_directory_uri() . '/js/small-menu.js', array( 'jquery' ), '20120206', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}
	// height matching script so #content and #sidebar are the same height
	wp_enqueue_script( 'height', get_template_directory_uri() . '/js/height.js' , array( 'jquery' ) );
	
}
add_action( 'wp_enqueue_scripts', 'ritchie_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );


/* 
 * Stripping the embedded height & width from Featured Images from http://wordpress.stackexchange.com/questions/5568/filter-to-remove-image-dimension-attributes 
 */

add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );

function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}