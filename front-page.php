<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package ritchie
 * @since ritchie 1.0
 */

get_header(); ?>
		<div id="main" class="site-main">
			<embed width="734" height="267" src="<?php echo get_stylesheet_directory_uri() ?>/inc/home_flash.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" class="front-flash">
			<div id="content" class="site-content" role="main">
				

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>

			</div><!-- #content .site-content -->
		</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>