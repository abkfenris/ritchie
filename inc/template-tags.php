<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package ritchie
 * @since ritchie 1.0
 */

if ( ! function_exists( 'ritchie_content_nav' ) ) :
/**
 * Display navigation to next/previous pages when applicable
 *
 * @since ritchie 1.0
 */
function ritchie_content_nav( $nav_id ) {
	global $wp_query, $post;

	// Don't print empty markup on single pages if there's nowhere to navigate.
	if ( is_single() ) {
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
	}

	// Don't print empty markup in archives if there's only one page.
	if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
		return;

	$nav_class = 'site-navigation paging-navigation';
	if ( is_single() )
		$nav_class = 'site-navigation post-navigation';

	?>
	<nav role="navigation" id="<?php echo $nav_id; ?>" class="<?php echo $nav_class; ?>">
		<h1 class="assistive-text"><?php _e( 'Post navigation', 'ritchie' ); ?></h1>

	<?php if ( is_single() ) : // navigation links for single posts ?>

		<?php previous_post_link( '<div class="nav-previous">%link</div>', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'ritchie' ) . '</span> %title' ); ?>
		<?php next_post_link( '<div class="nav-next">%link</div>', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'ritchie' ) . '</span>' ); ?>

	<?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>

		<?php if ( get_next_posts_link() ) : ?>
		<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'ritchie' ) ); ?></div>
		<?php endif; ?>

		<?php if ( get_previous_posts_link() ) : ?>
		<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'ritchie' ) ); ?></div>
		<?php endif; ?>

	<?php endif; ?>

	</nav><!-- #<?php echo $nav_id; ?> -->
	<?php
}
endif; // ritchie_content_nav

if ( ! function_exists( 'ritchie_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since ritchie 1.0
 */
function ritchie_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'ritchie' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'ritchie' ), ' ' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<footer>
				<div class="comment-author vcard">
					<?php echo get_avatar( $comment, 40 ); ?>
					<?php printf( __( '%s <span class="says">says:</span>', 'ritchie' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
				</div><!-- .comment-author .vcard -->
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em><?php _e( 'Your comment is awaiting moderation.', 'ritchie' ); ?></em>
					<br />
				<?php endif; ?>

				<div class="comment-meta commentmetadata">
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><time pubdate datetime="<?php comment_time( 'c' ); ?>">
					<?php
						/* translators: 1: date, 2: time */
						printf( __( '%1$s at %2$s', 'ritchie' ), get_comment_date(), get_comment_time() ); ?>
					</time></a>
					<?php edit_comment_link( __( '(Edit)', 'ritchie' ), ' ' );
					?>
				</div><!-- .comment-meta .commentmetadata -->
			</footer>

			<div class="comment-content"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
endif; // ends check for ritchie_comment()

if ( ! function_exists( 'ritchie_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 *
 * @since ritchie 1.0
 */
function ritchie_posted_on() {
	?>
		<a href="<?php echo get_permalink() ?>" title="<?php echo get_the_time() ?>" rel="bookmark">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/83-calendar.png" alt="83-calendar" width="23" height="25" class="type-icon"/> 
			<time class="entry-date" datetime="<?php echo get_the_date( 'c' ) ?>" pubdate="">
				<?php echo get_the_date() ?>
			</time>
		</a>
		<br />
		<span class="byline">
			<span class="author vcard">
				<a class="url fn n" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>" title="View all posts by <?php echo get_the_author() ?>" rel="author">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/111-user.png" alt="111-user" width="24" height="21" class="type-icon"/> 
					<?php echo get_the_author()?>
				</a>
			</span>
		</span>
	<?php
}
endif;

if ( ! function_exists( 'ritchie_horizontal_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 * horizontally
 * @since ritchie 1.0
 */
function ritchie_horizontal_posted_on() {
	?>
		<a href="<?php echo get_permalink() ?>" title="<?php echo get_the_time() ?>" rel="bookmark">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/83-calendar.png" alt="83-calendar" width="23" height="25" class="type-icon"/> 
			<time class="entry-date" datetime="<?php echo get_the_date( 'c' ) ?>" pubdate="">
				<?php echo get_the_date() ?>
			</time>
		</a> 
		<span class="byline">
			<span class="author vcard">
				<a class="url fn n" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>" title="View all posts by <?php echo get_the_author() ?>" rel="author">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/111-user.png" alt="111-user" width="24" height="21" class="type-icon"/> 
					<?php echo get_the_author()?>
				</a>
			</span>
		</span> 
	<?php	
}
endif;

if ( ! function_exists( 'ritchie_entry_meta' ) ) :
	function ritchie_entry_meta() {
		/* translators: used between list items, there is a space after the comma */
	    $category_list = get_the_category_list( __( ', ', 'ritchie' ) );

	    /* translators: used between list items, there is a space after the comma */
	    $tag_list = get_the_tag_list( '', __( ', ', 'ritchie' ) );

	    if ( ! ritchie_categorized_blog() ) {
	    	// This blog only has 1 category so we just need to worry about tags in the meta text
	    	if ( '' != $tag_list ) {
	    		// tags no categories
	    		?>
	    		<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/15-tags.png" alt="15-tags" width="24" height="25" class="type-icon"/> <?php echo get_the_tag_list( '', __( ', ', 'ritchie' ) ) ?> 
	    		<br />
	    		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"> 
	    			<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/01-anchor.png" alt="01-anchor" width="28" height="29" class="type-icon"/>
	    			 Bookmark the permalink
	    		</a><?php
	    		$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'ritchie' );
	    	} else {
	    		// no tags or categories
	    		?>
	    		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"> 
	    			<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/01-anchor.png" alt="01-anchor" width="28" height="29" class="type-icon"/>
	    			 Bookmark the permalink
	    		</a><?php
	    	}

	    } else {
	    	// But this blog has loads of categories so we should probably display them here
	    	if ( '' != $tag_list ) {
	    		// categories and tags
	    		
	    		?>
	    		<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/255-box.png" alt="255-box" width="24" height="22" class="type-icon"/> <?php echo get_the_category_list( __( ', ', 'ritchie' ) ) ?> 
	    		<br />
	    		<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/15-tags.png" alt="15-tags" width="24" height="25" class="type-icon"/> <?php echo get_the_tag_list( '', __( ', ', 'ritchie' ) ) ?> 
	    		<br />
	    		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"> 
	    			<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/01-anchor.png" alt="01-anchor" width="28" height="29" class="type-icon"/>
	    			 Bookmark the permalink
	    		</a><?php
	    	} else {
	    		// categories but no tags
	    	
	    		?>
	    		<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/255-box.png" alt="255-box" width="24" height="22" class="type-icon"/> <?php echo get_the_category_list( __( ', ', 'ritchie' ) ) ?> 
	    		<br />
	    		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"> 
	    			<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/01-anchor.png" alt="01-anchor" width="28" height="29" class="type-icon"/>
	    			 Bookmark the permalink
	    		</a><?php
	    	}

	    } // end check for categories on this blog
	}	
endif;

if ( ! function_exists( 'ritchie_horizontal_entry_meta' ) ) :
function ritchie_horizontal_entry_meta() {
	/* translators: used between list items, there is a space after the comma */
    $category_list = get_the_category_list( __( ', ', 'ritchie' ) );

    /* translators: used between list items, there is a space after the comma */
    $tag_list = get_the_tag_list( '', __( ', ', 'ritchie' ) );

    if ( ! ritchie_categorized_blog() ) {
    	// This blog only has 1 category so we just need to worry about tags in the meta text
    	if ( '' != $tag_list ) {
    		// tags no categories
    		$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'ritchie' );
    	} else {
    		// no tags or categories
    		$meta_text = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'ritchie' );
    	}

    } else {
    	// But this blog has loads of categories so we should probably display them here
    	if ( '' != $tag_list ) {
    		// categories and tags
    		
    		?>
    		<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/255-box.png" alt="255-box" width="24" height="22" class="type-icon"/> <?php echo get_the_category_list( __( ', ', 'ritchie' ) ) ?> 
    		<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/15-tags.png" alt="15-tags" width="24" height="25" class="type-icon"/> <?php echo get_the_tag_list( '', __( ', ', 'ritchie' ) ) ?> 
    		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"> 
    			<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/01-anchor.png" alt="01-anchor" width="28" height="29" class="type-icon"/>
    			 Bookmark the permalink
    		</a><?php
    		$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'ritchie' );
    	} else {
    		// categories but no tags
    	
    		?>
    		<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/255-box.png" alt="255-box" width="24" height="22" class="type-icon"/> <?php echo get_the_category_list( __( ', ', 'ritchie' ) ) ?>  
    		
    		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"> 
    			<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/01-anchor.png" alt="01-anchor" width="28" height="29" class="type-icon"/>
    			 Bookmark the permalink
    		</a><?php
    		$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'ritchie' );
    	}

    } // end check for categories on this blog
		
}

endif;

/**
 * Short meta information, just date and permalink
 *
 * @since ritchie 1.0
 */
if ( ! function_exists( 'ritchie_short_meta_horizontal' ) ):
function ritchie_short_meta_horizontal() {
	?>
	<a href="<?php echo get_permalink() ?>" title="<?php echo get_the_time() ?>" rel="bookmark">
    	<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/83-calendar.png" alt="83-calendar" width="23" height="25" class="type-icon"/> 
    	<time class="entry-date" datetime="<?php echo get_the_date( 'c' ) ?>" pubdate="">
    		<?php echo get_the_date() ?>
    	</time>
    </a>
	<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"> 
		<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/01-anchor.png" alt="01-anchor" width="28" height="29" class="type-icon"/>
    	 Bookmark the permalink
    </a><?php
	
}

endif;

if ( ! function_exists( 'ritchie_short_meta' ) ):
function ritchie_short_meta() {
	?>
	<a href="<?php echo get_permalink() ?>" title="<?php echo get_the_time() ?>" rel="bookmark">
    	<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/83-calendar.png" alt="83-calendar" width="23" height="25" class="type-icon"/> 
    	<time class="entry-date" datetime="<?php echo get_the_date( 'c' ) ?>" pubdate="">
    		<?php echo get_the_date() ?>
    	</time>
    </a>
    <br />
	<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"> 
		<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/01-anchor.png" alt="01-anchor" width="28" height="29" class="type-icon"/>
    	 Bookmark the permalink
    </a><?php
	
}

endif;

/**
 * Returns true if a blog has more than 1 category
 *
 * @since ritchie 1.0
 */
function ritchie_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so ritchie_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so ritchie_categorized_blog should return false
		return false;
	}
}

/**
 * Flush out the transients used in ritchie_categorized_blog
 *
 * @since ritchie 1.0
 */
function ritchie_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'ritchie_category_transient_flusher' );
add_action( 'save_post', 'ritchie_category_transient_flusher' );