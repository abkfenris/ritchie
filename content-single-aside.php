<?php
/**
 * @package ritchie
 * @since ritchie 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content left">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	<footer class="entry-meta right">
		<?php ritchie_posted_on(); ?>
	 	<br />
		<?php ritchie_entry_meta();	?>

		<?php edit_post_link( __( 'Edit', 'ritchie' ), '<br /><span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
