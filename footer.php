<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package ritchie
 * @since ritchie 1.0
 */
?>

	

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<?php do_action( 'ritchie_credits' ); ?>
			
		    <?php do_action( 'before_sidebar' ); ?>
    	    <?php if ( ! dynamic_sidebar( 'sidebar-2' ) ) : ?>
    	
    	    	<aside id="search" class="widget widget_search">
    	    		<?php get_search_form(); ?>
    	    	</aside>
    	
    	    	<aside id="archives" class="widget">
    	    		<h1 class="widget-title"><?php _e( 'Archives', 'ritchie' ); ?></h1>
    	    		<ul>
    	    			<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
    	    		</ul>
    	    	</aside>
    	
    	    	<aside id="meta" class="widget">
    	    		<h1 class="widget-title"><?php _e( 'Meta', 'ritchie' ); ?></h1>
    	    		<ul>
    	    			<?php wp_register(); ?>
    	    			<li><?php wp_loginout(); ?></li>
    	    			<?php wp_meta(); ?>
    	    		</ul>
    	    	</aside>
    	
    	    <?php endif; // end footer widget area ?>
 
			<div class="credits">
				Design: <a href="http://www.southyarddigital.com">South Yard Design and Digital</a>
				<br />
				Powered by: <a href="http://wordpress.org">WordPress</a>
				<br />
				Code: <a href="http://alexkerney.com">Alex Kerney</a>
			</div>

		</div><!-- .site-info -->
	</footer><!-- #colophon .site-footer -->
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>

</body>
</html>