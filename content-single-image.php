<?php
/**
 * @package ritchie
 * @since ritchie 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php echo get_the_post_thumbnail($post->ID,'front-image'); ?>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ritchie' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/inc/icons-orange/121-landscape.png" alt="121-landscape" class="type-icon"/>
				<?php the_title(); ?>
			</a>
		</h1>	
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'ritchie' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php ritchie_horizontal_posted_on(); ?>
		<?php ritchie_horizontal_entry_meta();	?>
		<?php edit_post_link( __( 'Edit', 'ritchie' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
